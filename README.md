# 本科-毕业设计

> 基于语义分割的肝脏病变检测系统


## 目标与预测
1. 实现CT图像的分割
2. 检测出病变的部分


## 难点和实现方式

* C#实现主要界面
* python实现语义分割
* C++对CT图像数据进行处理
